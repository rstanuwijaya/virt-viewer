# Virt Viewer package strings.
# Copyright (C) 2019 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Chandan kumar <chandankumar.093047@gmail.com>, 2012
# rajesh <rajeshkajha@yahoo.com>, 2012
# Rajesh Ranjan <rajesh672@gmail.com>, 2010
# Rajesh Ranjan <rranjan@redhat.com>, 2012, 2013
msgid ""
msgstr ""
"Project-Id-Version: virt-viewer 9.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-28 11:18+0200\n"
"PO-Revision-Date: 2015-02-20 08:09+0000\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hindi (http://www.transifex.com/projects/p/virt-viewer/"
"language/hi/)\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

#, c-format
msgid ""
"\n"
"Error: can't handle multiple URIs\n"
"\n"
msgstr ""

#, c-format
msgid ""
"\n"
"No ID|UUID|DOMAIN-NAME was specified for '%s'\n"
"\n"
msgstr ""

#, c-format
msgid ""
"\n"
"Usage: %s [OPTIONS] [ID|UUID|DOMAIN-NAME]\n"
"\n"
msgstr ""

#. translators:
#. * This is "<ungrab accelerator> <subtitle> - <appname>"
#. * Such as: "(Press Ctrl+Alt to release pointer) BigCorpTycoon MOTD - Virt Viewer"
#.
#, c-format
msgid "%s %s - %s"
msgstr ""

#. translators:
#. * This is "<ungrab accelerator> - <appname>"
#. * Such as: "(Press Ctrl+Alt to release pointer) - Virt Viewer"
#.
#. translators:
#. * This is "<subtitle> - <appname>"
#. * Such as: "BigCorpTycoon MOTD - Virt Viewer"
#.
#, c-format
msgid "%s - %s"
msgstr ""

#, c-format
msgid "%s version %s"
msgstr ""

#, c-format
msgid "(Press %s to release pointer)"
msgstr "(सूचक जारी करने के लिए %s दबाएँ)"

msgid "<b>Loading...</b>"
msgstr ""

msgid "<never|always>"
msgstr ""

msgid "<never|on-disconnect>"
msgstr ""

msgid "A remote desktop client built with GTK-VNC, SPICE-GTK and libvirt"
msgstr "एक दूरस्थ डेस्कटॉप ग्राहक जीतीके-वीएनसी, स्पाइस-जीटीके और लिब्विर्ट के साथ निर्मित"

msgid "About Virt-Viewer"
msgstr ""

msgid "Access remote desktops"
msgstr "दूरस्थ डेस्कटॉप की पहुँच लें"

#, c-format
msgid "Address is too long for UNIX socket_path: %s"
msgstr ""

#, c-format
msgid "At least %s version %s is required to setup this connection"
msgstr "कम से कम %s संस्करण %s इस कनेक्शन के सेटअप के लिए जरूरी है"

#, c-format
msgid ""
"At least %s version %s is required to setup this connection, see %s for "
"details"
msgstr ""

msgid "Attach to the local display using libvirt"
msgstr "लिब्विर्ट का उपयोग करके स्थानीय डिस्प्ले का संलग्न करें"

msgid "Authentication failed."
msgstr ""

#, c-format
msgid ""
"Authentication is required for the %s connection to:\n"
"\n"
"<b>%s</b>\n"
"\n"
msgstr ""

#, c-format
msgid "Authentication is required for the %s connection:\n"
msgstr ""

msgid "Authentication required"
msgstr "सत्यापन की आवश्यकता"

msgid "Authentication was cancelled"
msgstr ""

msgid "Automatically resize remote framebuffer"
msgstr ""

msgid "Available virtual machines"
msgstr ""

msgid "C_onnect"
msgstr ""

#, c-format
msgid "Can't connect to channel: %s"
msgstr ""

msgid "Cannot determine the connection type from URI"
msgstr "यूआरआइ से कनेक्शन के प्रकार को तय नहीं कर सकता है"

#, c-format
msgid "Cannot determine the graphic type for the guest %s"
msgstr "%s अतिथि के लिए ग्राफ़िक प्रकार निर्धारित नहीं कर सकते है "

#, c-format
msgid "Cannot determine the host for the guest %s"
msgstr "%s अतिथि के लिए होस्ट का निर्धारण नहीं कर सकते है "

msgid "Cannot get guest state"
msgstr ""

msgid "Change CD"
msgstr ""

msgid "Checking guest domain status"
msgstr "अतिथि डोमेन की स्थिति की जाँच हो रही है"

msgid "Choose a virtual machine"
msgstr ""

msgid "Connect to SSH failed."
msgstr ""

msgid "Connect to channel unsupported."
msgstr " चैनल से कनेक्ट करना असहायक"

msgid "Connect to hypervisor"
msgstr "हाइपरविजर से कनेक्ट करें"

msgid "Connected to graphic server"
msgstr "ग्राफ़िक सर्वर से जुड़ा"

#, c-format
msgid "Connecting to UNIX socket failed: %s"
msgstr ""

msgid "Connecting to graphic server"
msgstr "ग्राफ़िक सर्वर से कनेक्ट हो रहा है."

msgid "Connection _Address"
msgstr ""

msgid "Connection details"
msgstr "संबंध विवरण"

msgid "Console support is compiled out!"
msgstr ""

msgid ""
"Copyright (C) 2007-2012 Daniel P. Berrange\n"
"Copyright (C) 2007-2020 Red Hat, Inc."
msgstr ""

msgid "Couldn't open oVirt session: "
msgstr ""

#, c-format
msgid "Creating UNIX socket failed: %s"
msgstr ""

#, c-format
msgid "Current: %s"
msgstr ""

msgid "Cursor display mode: 'local' or 'auto'"
msgstr ""

msgid "Customise hotkeys"
msgstr "हॉटकी मनपसंद बनाएँ"

msgctxt "shortcut window"
msgid "Devices"
msgstr ""

msgid "Direct connection with no automatic tunnels"
msgstr " स्वचालित सुरंगों के साथ कोई भी प्रत्यक्ष कनेक्शन नहीं "

#, c-format
msgid "Display _%d"
msgstr ""

msgid "Display can only be attached through libvirt with --attach"
msgstr ""

msgid "Display debugging information"
msgstr "डिबगिंग सूचना दिखाता है"

msgid "Display verbose information"
msgstr " वर्बोज की जानकारी दिखाता है"

msgid "Display version information"
msgstr "संस्करण की जानकारी दिखाता है"

msgid "Do not ask me again"
msgstr "मुझे फिर मत पूछें"

msgid "Do you want to close the session?"
msgstr "क्या आप इस सत्र को आयात बंद करना चाहते हैं?"

msgid "Enable kiosk mode"
msgstr ""

msgid "Failed to change CD"
msgstr ""

msgid "Failed to connect: "
msgstr ""

msgid "Failed to initiate connection"
msgstr "कनेक्शन आरंभ करने में विफल"

msgid "Failed to read stdin: "
msgstr ""

msgid "File Transfers"
msgstr ""

msgid "Finding guest domain"
msgstr "अतिथि डोमेन को खोज रहा है "

msgid "Folder sharing"
msgstr ""

msgid "For example, spice://foo.example.org:5900"
msgstr ""

msgctxt "shortcut window"
msgid "Fullscreen"
msgstr ""

msgid "GUID:"
msgstr ""

#, c-format
msgid "Guest '%s' is not reachable"
msgstr ""

msgid "Guest Details"
msgstr ""

msgid "Guest domain has shutdown"
msgstr "अतिथि के डोमेन को शटडाउन किया है"

msgctxt "shortcut window"
msgid "Guest input devices"
msgstr ""

#, c-format
msgid "Invalid file %s: "
msgstr ""

#, c-format
msgid "Invalid kiosk-quit argument: %s"
msgstr ""

msgid "Invalid password"
msgstr ""

msgid "Leave fullscreen"
msgstr "फुल स्क्रीन छोड़ दें"

msgid "Loading..."
msgstr ""

msgid "Machine"
msgstr ""

msgid "More actions"
msgstr ""

msgid "Name"
msgstr ""

msgid "Name:"
msgstr ""

msgid "No ISO files in domain"
msgstr ""

msgid "No connection was chosen"
msgstr ""

msgid "No running virtual machine found"
msgstr ""

msgid "No virtual machine was chosen"
msgstr ""

msgid "Open in full screen mode (adjusts guest resolution to fit the client)"
msgstr ""

msgid "Password:"
msgstr "कूटशब्द:"

msgid "Please add an extension to the file name"
msgstr ""

msgid "Power _down"
msgstr ""

msgid "Preferences"
msgstr ""

msgid "QEMU debug console"
msgstr ""

msgid "QEMU human monitor"
msgstr ""

msgid "Quit on given condition in kiosk mode"
msgstr ""

msgid "Read-only"
msgstr ""

msgid "Recent connections"
msgstr ""

msgid "Reconnect to domain upon restart"
msgstr "पुनः आरंभ होने पर डोमेन को फिर से जुड़ें"

msgid "Refresh"
msgstr ""

msgctxt "shortcut window"
msgid "Release cursor"
msgstr ""

msgid ""
"Remap keys format key=keymod+key e.g. F1=SHIFT+CTRL+F1,1=SHIFT+F1,ALT_L=Void"
msgstr ""

msgid "Remote Viewer"
msgstr "दूरस्थ दर्शक"

msgid ""
"Remote Viewer provides a graphical viewer for the guest OS display. At this "
"time it supports guest OS using the VNC or SPICE protocols. Further "
"protocols may be supported in the future as user demand dictates. The viewer "
"can connect directly to both local and remotely hosted guest OS, optionally "
"using SSL/TLS encryption."
msgstr ""

msgid "Remote viewer client"
msgstr ""

msgid "Remotely access virtual machines"
msgstr ""

#, c-format
msgid "Run '%s --help' to see a full list of available command line options\n"
msgstr ""

msgid "Save screenshot"
msgstr ""

msgid "Screenshot.png"
msgstr ""

#. Create the widgets
msgid "Select USB devices for redirection"
msgstr "पुनर्निर्देशन के लिए यूएसबी उपकरणों का चयन करें"

msgid "Select the virtual machine only by its ID"
msgstr ""

msgid "Select the virtual machine only by its UUID"
msgstr ""

msgid "Select the virtual machine only by its name"
msgstr ""

msgid "Selected"
msgstr ""

msgid "Send key"
msgstr ""

msgctxt "shortcut window"
msgid "Send secure attention (Ctrl-Alt-Del)"
msgstr ""

msgid "Serial"
msgstr ""

msgid "Set window title"
msgstr "विंडो शीर्षक सेट करें"

msgid "Share client session"
msgstr ""

msgid "Share clipboard"
msgstr ""

msgid "Share folder"
msgstr ""

msgid "Show / hide password text"
msgstr ""

msgctxt "shortcut window"
msgid "Smartcard insert"
msgstr ""

msgctxt "shortcut window"
msgid "Smartcard remove"
msgstr ""

msgid "Spice"
msgstr ""

msgid "Switch to fullscreen view"
msgstr ""

msgid "The Fedora Translation Team"
msgstr "फेडोरा अनुवाद टीम"

#, c-format
msgid "Transferring %u file of %u..."
msgid_plural "Transferring %u files of %u..."
msgstr[0] ""
msgstr[1] ""

msgid "Transferring 1 file..."
msgstr ""

msgctxt "shortcut window"
msgid "USB device reset"
msgstr ""

msgid "USB device selection"
msgstr "यूएसबी उपकरण चयन"

#, c-format
msgid "USB redirection error: %s"
msgstr "यूएसबी पुनर्निर्देशन त्रुटि : %s"

#, c-format
msgid "Unable to authenticate with remote desktop server at %s: %s\n"
msgstr ""

#, c-format
msgid "Unable to authenticate with remote desktop server: %s"
msgstr "दूरस्थ डेस्कटॉप सर्वर के साथ सत्यापन करने में असमर्थ : %s"

#, c-format
msgid "Unable to connect to libvirt with URI: %s."
msgstr ""

#, c-format
msgid "Unable to connect to the graphic server %s"
msgstr "%s ग्राफ़िक सर्वर से कनेक्ट करने में असमर्थ"

#, c-format
msgid "Unable to connect: %s"
msgstr ""

msgid "Unable to connnect to oVirt"
msgstr ""

#, c-format
msgid "Unable to determine image format for file '%s'"
msgstr ""

msgctxt "Unknown UUID"
msgid "Unknown"
msgstr ""

msgctxt "Unknown name"
msgid "Unknown"
msgstr ""

msgid "Unspecified error"
msgstr ""

#, c-format
msgid "Unsupported authentication type %u"
msgstr ""

#, c-format
msgid "Unsupported graphic type '%s'"
msgstr ""

msgid "Username:"
msgstr "उपयोगकर्ता नाम:"

msgid "VNC does not provide GUID"
msgstr ""

msgctxt "shortcut window"
msgid "Viewer"
msgstr ""

msgid "Virt Viewer"
msgstr "वर्चुअल व्यूअर"

msgid "Virt-Viewer connection file"
msgstr ""

#, c-format
msgid "Virtual machine %s is not running"
msgstr ""

msgid "Virtual machine graphical console"
msgstr ""

msgid "Wait for domain to start"
msgstr "डोमेन को प्रारंभ करने के लिए प्रतीक्षा करें"

#, c-format
msgid "Waiting for display %d..."
msgstr " %d प्रदर्शन के लिए प्रतीक्षा कर रहा है ..."

msgid "Waiting for guest domain to be created"
msgstr "अतिथि डोमेन बनाई जाने प्रतीक्षा के लिए की जा रही है"

msgid "Waiting for guest domain to re-start"
msgstr "अतिथि डोमेन को फिर से प्रारंभ करने के लिए प्रतीक्षा करे "

msgid "Waiting for guest domain to start"
msgstr "  अतिथि डोमेन शुरू करने के लिए प्रतीक्षा की जा रही है "

msgid "Waiting for libvirt to start"
msgstr "libvirt शुरू करने के लिए प्रतीक्षा की जा रही है"

msgid "Zoom _In"
msgstr ""

msgid "Zoom _Out"
msgstr ""

msgctxt "shortcut window"
msgid "Zoom in"
msgstr ""

#, c-format
msgid "Zoom level must be within %d-%d\n"
msgstr ""

msgid "Zoom level of window, in percentage"
msgstr "विंडो का जूम स्तर, प्रतिशत में"

msgctxt "shortcut window"
msgid "Zoom out"
msgstr ""

msgctxt "shortcut window"
msgid "Zoom reset"
msgstr ""

msgid "[none]"
msgstr "कुछ नहीं"

msgid "_About"
msgstr ""

msgid "_Auto resize"
msgstr ""

msgid "_Cancel"
msgstr ""

msgid "_Close"
msgstr ""

msgid "_Guest details"
msgstr ""

msgid "_Keyboard shortcuts"
msgstr ""

msgid "_Normal Size"
msgstr ""

msgid "_OK"
msgstr ""

msgid "_Pause"
msgstr ""

msgid "_Preferences…"
msgstr ""

msgid "_Reset"
msgstr ""

msgid "_Save"
msgstr ""

msgid "_Screenshot"
msgstr ""

msgid "failed to parse oVirt URI"
msgstr ""

msgid "label"
msgstr "लेबल"

#, c-format
msgid "oVirt VM %s has no display"
msgstr ""

#, c-format
msgid "oVirt VM %s has no host information"
msgstr ""

#, c-format
msgid "oVirt VM %s has unknown display type: %u"
msgstr ""

#, c-format
msgid "oVirt VM %s is not running"
msgstr ""

msgid "only SSH or UNIX socket connection supported."
msgstr ""

#~ msgid "Disconnect"
#~ msgstr "डिस्कनेक्ट करें"

#~ msgid "Release cursor"
#~ msgstr "रिलीज कर्सर"

#~ msgid "Send key combination"
#~ msgstr "कुंजी संयोजन भेजें"

#~ msgid "Smartcard insertion"
#~ msgstr "स्मार्टकार्ड प्रवेश"

#~ msgid "Smartcard removal"
#~ msgstr "स्मार्टकार्ड हटाना"

#~ msgid "_File"
#~ msgstr "फाइल (_F)"

#~ msgid "_Help"
#~ msgstr "सहायता (_H)"

#~ msgid "_Send key"
#~ msgstr "कुंजी भेजें (_S)"

#~ msgid "_View"
#~ msgstr "दृश्य (_V)"

#~ msgid "_Zoom"
#~ msgstr "ज़ूम (_Z)"

#~ msgid " "
#~ msgstr "  "

#~ msgid "%s%s%s - %s"
#~ msgstr "%s%s%s - %s"

#~ msgid "Connect to ssh failed."
#~ msgstr "सश से कनेक्ट करने में विफल."

#~ msgid "Ctrl+Alt"
#~ msgstr "Ctrl+Alt"

#~ msgid "Ctrl+Alt+F11"
#~ msgstr "Ctrl+Alt+F11"

#~ msgid "Ctrl+Alt+F12"
#~ msgstr "Ctrl+Alt+F12"

#~ msgid "Ctrl+Alt+F1_0"
#~ msgstr "Ctrl+Alt+F1_0"

#~ msgid "Ctrl+Alt+F_1"
#~ msgstr "Ctrl+Alt+F_1"

#~ msgid "Ctrl+Alt+F_2"
#~ msgstr "Ctrl+Alt+F_2"

#~ msgid "Ctrl+Alt+F_3"
#~ msgstr "Ctrl+Alt+F_3"

#~ msgid "Ctrl+Alt+F_4"
#~ msgstr "Ctrl+Alt+F_4"

#~ msgid "Ctrl+Alt+F_5"
#~ msgstr "Ctrl+Alt+F_5"

#~ msgid "Ctrl+Alt+F_6"
#~ msgstr "Ctrl+Alt+F_6"

#~ msgid "Ctrl+Alt+F_7"
#~ msgstr "Ctrl+Alt+F_7"

#~ msgid "Ctrl+Alt+F_8"
#~ msgstr "Ctrl+Alt+F_8"

#~ msgid "Ctrl+Alt+F_9"
#~ msgstr "Ctrl+Alt+F_9"

#~ msgid "Ctrl+Alt+_Backspace"
#~ msgstr "Ctrl+Alt+_Backspace"

#~ msgid "Ctrl+Alt+_Del"
#~ msgstr "Ctrl+Alt+_Del"

#~ msgid ""
#~ "This program is free software; you can redistribute it and/or modify\n"
#~ "it under the terms of the GNU General Public License as published by\n"
#~ "the Free Software Foundation; either version 2 of the License, or\n"
#~ "(at your option) any later version.\n"
#~ "\n"
#~ "This program is distributed in the hope that it will be useful,\n"
#~ "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
#~ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
#~ "GNU General Public License for more details.\n"
#~ "\n"
#~ "You should have received a copy of the GNU General Public License\n"
#~ "along with this program; if not, write to the Free Software\n"
#~ "Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  "
#~ "USA\n"
#~ msgstr ""
#~ "यह प्रोग्राम मुफ्त सॉफ्टवेयर का है: आप इसे फ्री सॉफ्टवेयर फाउंडेशन के द्वारा प्रकाशित "
#~ "जीएनयू जनरल पब्लिक लाइसेंस; या तो लाइसेंस का संस्करण 2, या (आपके विकल्प के अनुसार) "
#~ "बाद के किसी संस्करण की शर्तों के तहत  पुनर्वितरित और / संशोधित कर सकते हैं .\n"
#~ "\n"
#~ "इस कार्यक्रम को इस उम्मीद से वितरित किया गया है कि यह बिना किसी वारंटी;"
#~ "व्यापारिकता की अप्रत्यक्ष वारंटी या किसी खास उद्देश्य के लिए उपयुक्तता के बिना उपयोगी "
#~ "होगा.    अधिक जानकारी के लिए जीएनयू जनरल पब्लिक लाइसेंस देखें.\n"
#~ "\n"
#~ "आप इस प्रोग्राम के साथ जीएनयू जनरल पब्लिक लाइसेंस की एक प्रतिलिपि प्राप्त करेगे; अगर "
#~ "नहीं, फ्री सॉफ्टवेयर फाउंडेशन को लिखे, Inc. 59 मंदिर प्लेस, 330 सूट, बोस्टन, एमए    "
#~ "02111-1307, संयुक्त राज्य अमेरिका\n"

#~ msgid "virt-manager.org"
#~ msgstr "virt-manager.org"
