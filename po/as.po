# Virt Viewer package strings.
# Copyright (C) 2019 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Amitakhya Phukan <aphukan@redhat.com>, 2010
# Nilamdyuti Goswami <ngoswami@redhat.com>, 2012, 2013, 2014
msgid ""
msgstr ""
"Project-Id-Version: virt-viewer 9.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-28 11:18+0200\n"
"PO-Revision-Date: 2015-02-20 08:08+0000\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Assamese (http://www.transifex.com/projects/p/virt-viewer/"
"language/as/)\n"
"Language: as\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

#, c-format
msgid ""
"\n"
"Error: can't handle multiple URIs\n"
"\n"
msgstr ""

#, c-format
msgid ""
"\n"
"No ID|UUID|DOMAIN-NAME was specified for '%s'\n"
"\n"
msgstr ""

#, c-format
msgid ""
"\n"
"Usage: %s [OPTIONS] [ID|UUID|DOMAIN-NAME]\n"
"\n"
msgstr ""

#. translators:
#. * This is "<ungrab accelerator> <subtitle> - <appname>"
#. * Such as: "(Press Ctrl+Alt to release pointer) BigCorpTycoon MOTD - Virt Viewer"
#.
#, c-format
msgid "%s %s - %s"
msgstr ""

#. translators:
#. * This is "<ungrab accelerator> - <appname>"
#. * Such as: "(Press Ctrl+Alt to release pointer) - Virt Viewer"
#.
#. translators:
#. * This is "<subtitle> - <appname>"
#. * Such as: "BigCorpTycoon MOTD - Virt Viewer"
#.
#, c-format
msgid "%s - %s"
msgstr ""

#, c-format
msgid "%s version %s"
msgstr ""

#, c-format
msgid "(Press %s to release pointer)"
msgstr "(পোইন্টাৰ এৰিবলে %s টিপক)"

msgid "<b>Loading...</b>"
msgstr ""

msgid "<never|always>"
msgstr ""

msgid "<never|on-disconnect>"
msgstr "<never|on-disconnect>"

msgid "A remote desktop client built with GTK-VNC, SPICE-GTK and libvirt"
msgstr ""
"GTK-VNC, SPICE-GTK আৰু libvirt ৰ সৈতে নিৰ্মাণ কৰা এটা দূৰৱৰ্তী ডেস্কটপ ক্লাএন্ট"

msgid "About Virt-Viewer"
msgstr ""

msgid "Access remote desktops"
msgstr "দূৰৱৰ্তী ডেস্কটপসমূহ অভিগম কৰক"

#, c-format
msgid "Address is too long for UNIX socket_path: %s"
msgstr ""

#, c-format
msgid "At least %s version %s is required to setup this connection"
msgstr "এই সংযোগ সংস্থাপন কৰিবলে অন্তত %s সংস্কৰণ %s ৰ প্ৰয়োজন"

#, c-format
msgid ""
"At least %s version %s is required to setup this connection, see %s for "
"details"
msgstr ""

msgid "Attach to the local display using libvirt"
msgstr "libvirt ব্যৱহাৰ কৰি স্থানীয় প্ৰদৰ্শনলে সংলঘ্ন কৰক"

msgid "Authentication failed."
msgstr ""

#, c-format
msgid ""
"Authentication is required for the %s connection to:\n"
"\n"
"<b>%s</b>\n"
"\n"
msgstr ""

#, c-format
msgid "Authentication is required for the %s connection:\n"
msgstr ""

msgid "Authentication required"
msgstr "প্ৰমাণীকৰণৰ প্ৰয়োজন"

msgid "Authentication was cancelled"
msgstr ""

msgid "Automatically resize remote framebuffer"
msgstr ""

msgid "Available virtual machines"
msgstr ""

msgid "C_onnect"
msgstr ""

#, c-format
msgid "Can't connect to channel: %s"
msgstr ""

msgid "Cannot determine the connection type from URI"
msgstr "URI ৰ পৰা সংযোগ ধৰণ নিৰ্ধাৰণ কৰিব পৰা নগল"

#, c-format
msgid "Cannot determine the graphic type for the guest %s"
msgstr "অতিথি %s ৰ বাবে গ্ৰাফিক ধৰণ নিৰ্ধাৰণ কৰিব নোৱাৰি"

#, c-format
msgid "Cannot determine the host for the guest %s"
msgstr "অতিথি %s ৰ বাবে হস্ট নিৰ্ধাৰণ কৰিব নোৱাৰি"

msgid "Cannot get guest state"
msgstr ""

msgid "Change CD"
msgstr ""

msgid "Checking guest domain status"
msgstr "অতিথি ডমেইন অৱস্থা নীৰিক্ষণ কৰা"

msgid "Choose a virtual machine"
msgstr ""

msgid "Connect to SSH failed."
msgstr ""

msgid "Connect to channel unsupported."
msgstr "চেনেললে সংযোগ অসমৰ্থিত।"

msgid "Connect to hypervisor"
msgstr "হাইপাৰভাইছৰলে সংযোগ কৰক"

msgid "Connected to graphic server"
msgstr "গ্ৰাফিক চাৰ্ভাৰলে সংযোগিত"

#, c-format
msgid "Connecting to UNIX socket failed: %s"
msgstr ""

msgid "Connecting to graphic server"
msgstr "গ্ৰাফিক চাৰ্ভাৰলে সংযোগ কৰা হৈ আছে"

msgid "Connection _Address"
msgstr ""

msgid "Connection details"
msgstr "সংযোগৰ বিৱৰণসমূহ"

msgid "Console support is compiled out!"
msgstr ""

msgid ""
"Copyright (C) 2007-2012 Daniel P. Berrange\n"
"Copyright (C) 2007-2020 Red Hat, Inc."
msgstr ""

msgid "Couldn't open oVirt session: "
msgstr ""

#, c-format
msgid "Creating UNIX socket failed: %s"
msgstr ""

#, c-format
msgid "Current: %s"
msgstr ""

msgid "Cursor display mode: 'local' or 'auto'"
msgstr ""

msgid "Customise hotkeys"
msgstr "হটকি'সমূহ স্বনিৰ্বাচন কৰক"

msgctxt "shortcut window"
msgid "Devices"
msgstr ""

msgid "Direct connection with no automatic tunnels"
msgstr "কোনো স্বচালিত টানেল নহোৱাকৈ প্ৰত্যক্ষ সংযোগ"

#, c-format
msgid "Display _%d"
msgstr ""

msgid "Display can only be attached through libvirt with --attach"
msgstr ""

msgid "Display debugging information"
msgstr "ডিবাগ তথ্য প্ৰদৰ্শন কৰক"

msgid "Display verbose information"
msgstr "ভাৰভৌচ তথ্য প্ৰদৰ্শন কৰক"

msgid "Display version information"
msgstr "সংস্কৰণ তথ্য প্ৰদৰ্শন কৰক"

msgid "Do not ask me again"
msgstr "মোক আকৌ নুশুধিব"

msgid "Do you want to close the session?"
msgstr "আপুনি অধিবেশন বন্ধ কৰিব বিচাৰে নে?"

msgid "Enable kiosk mode"
msgstr "কিঅস্ক অৱস্থা সামৰ্থবান কৰক"

msgid "Failed to change CD"
msgstr ""

msgid "Failed to connect: "
msgstr ""

msgid "Failed to initiate connection"
msgstr "সংযোগ আৰম্ভ কৰিবলে ব্যৰ্থ"

msgid "Failed to read stdin: "
msgstr ""

msgid "File Transfers"
msgstr ""

msgid "Finding guest domain"
msgstr "অতিথি ডমেইন বিচৰা"

msgid "Folder sharing"
msgstr ""

msgid "For example, spice://foo.example.org:5900"
msgstr "উদাহৰণস্বৰূপ, spice://foo.example.org:5900"

msgctxt "shortcut window"
msgid "Fullscreen"
msgstr ""

msgid "GUID:"
msgstr ""

#, c-format
msgid "Guest '%s' is not reachable"
msgstr ""

msgid "Guest Details"
msgstr ""

msgid "Guest domain has shutdown"
msgstr "অতিথি ডমেইন বন্ধ হল"

msgctxt "shortcut window"
msgid "Guest input devices"
msgstr ""

#, c-format
msgid "Invalid file %s: "
msgstr ""

#, c-format
msgid "Invalid kiosk-quit argument: %s"
msgstr "অবৈধ kiosk-quit তৰ্ক: %s"

msgid "Invalid password"
msgstr ""

msgid "Leave fullscreen"
msgstr "পূৰ্ণপৰ্দা ত্যাগ কৰক"

msgid "Loading..."
msgstr ""

msgid "Machine"
msgstr ""

msgid "More actions"
msgstr ""

msgid "Name"
msgstr ""

msgid "Name:"
msgstr ""

msgid "No ISO files in domain"
msgstr ""

msgid "No connection was chosen"
msgstr ""

msgid "No running virtual machine found"
msgstr ""

msgid "No virtual machine was chosen"
msgstr ""

msgid "Open in full screen mode (adjusts guest resolution to fit the client)"
msgstr ""
"পূৰ্ণ পৰ্দা অৱস্থাত খোলক (ক্লাএন্টৰ সৈতে খাপ খোৱাবলে অতিথিৰ বিভেদন ধাৰ্য্য কৰে)"

msgid "Password:"
msgstr "পাছৱাৰ্ড:"

msgid "Please add an extension to the file name"
msgstr ""

msgid "Power _down"
msgstr ""

msgid "Preferences"
msgstr ""

msgid "QEMU debug console"
msgstr ""

msgid "QEMU human monitor"
msgstr ""

msgid "Quit on given condition in kiosk mode"
msgstr "কিঅস্ক অৱস্থাত দিয়া চুক্তিত প্ৰস্থান কৰিব"

msgid "Read-only"
msgstr ""

msgid "Recent connections"
msgstr ""

msgid "Reconnect to domain upon restart"
msgstr "পুনৰাম্ভ হওতে ডমেইনলে পুনৰসংযোগ কৰক"

msgid "Refresh"
msgstr ""

msgctxt "shortcut window"
msgid "Release cursor"
msgstr ""

msgid ""
"Remap keys format key=keymod+key e.g. F1=SHIFT+CTRL+F1,1=SHIFT+F1,ALT_L=Void"
msgstr ""

msgid "Remote Viewer"
msgstr "দূৰৱৰ্তী দৰ্শক"

msgid ""
"Remote Viewer provides a graphical viewer for the guest OS display. At this "
"time it supports guest OS using the VNC or SPICE protocols. Further "
"protocols may be supported in the future as user demand dictates. The viewer "
"can connect directly to both local and remotely hosted guest OS, optionally "
"using SSL/TLS encryption."
msgstr ""

msgid "Remote viewer client"
msgstr "দূৰৱৰ্তী দৰ্শক ক্লাএণ্ট"

msgid "Remotely access virtual machines"
msgstr ""

#, c-format
msgid "Run '%s --help' to see a full list of available command line options\n"
msgstr ""

msgid "Save screenshot"
msgstr ""

msgid "Screenshot.png"
msgstr ""

#. Create the widgets
msgid "Select USB devices for redirection"
msgstr "পুনৰনিৰ্দেশৰ বাবে USB ডিভাইচসমূহ বাছক"

msgid "Select the virtual machine only by its ID"
msgstr ""

msgid "Select the virtual machine only by its UUID"
msgstr ""

msgid "Select the virtual machine only by its name"
msgstr ""

msgid "Selected"
msgstr ""

msgid "Send key"
msgstr ""

msgctxt "shortcut window"
msgid "Send secure attention (Ctrl-Alt-Del)"
msgstr ""

msgid "Serial"
msgstr ""

msgid "Set window title"
msgstr "উইন্ডো শীৰ্ষক সংহতি কৰক"

msgid "Share client session"
msgstr ""

msgid "Share clipboard"
msgstr ""

msgid "Share folder"
msgstr ""

msgid "Show / hide password text"
msgstr ""

msgctxt "shortcut window"
msgid "Smartcard insert"
msgstr ""

msgctxt "shortcut window"
msgid "Smartcard remove"
msgstr ""

msgid "Spice"
msgstr ""

msgid "Switch to fullscreen view"
msgstr ""

msgid "The Fedora Translation Team"
msgstr "Fedora অনুবাদ দল"

#, c-format
msgid "Transferring %u file of %u..."
msgid_plural "Transferring %u files of %u..."
msgstr[0] ""
msgstr[1] ""

msgid "Transferring 1 file..."
msgstr ""

msgctxt "shortcut window"
msgid "USB device reset"
msgstr ""

msgid "USB device selection"
msgstr "USB ডিভাইছ নিৰ্বাচন"

#, c-format
msgid "USB redirection error: %s"
msgstr "USB পুনৰনিৰ্দেশ ত্ৰুটি: %s"

#, c-format
msgid "Unable to authenticate with remote desktop server at %s: %s\n"
msgstr ""

#, c-format
msgid "Unable to authenticate with remote desktop server: %s"
msgstr "দূৰৱৰ্তী ডেস্কটপ চাৰ্ভাৰৰ সৈতে প্ৰমাণিত কৰিবলে অক্ষম: %s"

#, c-format
msgid "Unable to connect to libvirt with URI: %s."
msgstr ""

#, c-format
msgid "Unable to connect to the graphic server %s"
msgstr "গ্ৰাফিক চাৰ্ভাৰ %s লে সংযোগ কৰিবলে অক্ষম"

#, c-format
msgid "Unable to connect: %s"
msgstr ""

msgid "Unable to connnect to oVirt"
msgstr ""

#, c-format
msgid "Unable to determine image format for file '%s'"
msgstr ""

msgctxt "Unknown UUID"
msgid "Unknown"
msgstr ""

msgctxt "Unknown name"
msgid "Unknown"
msgstr ""

msgid "Unspecified error"
msgstr ""

#, c-format
msgid "Unsupported authentication type %u"
msgstr ""

#, c-format
msgid "Unsupported graphic type '%s'"
msgstr ""

msgid "Username:"
msgstr "ব্যৱহাৰকাৰীৰ নাম:"

msgid "VNC does not provide GUID"
msgstr ""

msgctxt "shortcut window"
msgid "Viewer"
msgstr ""

msgid "Virt Viewer"
msgstr "Virt দৰ্শক"

msgid "Virt-Viewer connection file"
msgstr ""

#, c-format
msgid "Virtual machine %s is not running"
msgstr ""

msgid "Virtual machine graphical console"
msgstr "ভাৰছুৱেল মেচিন গ্ৰাফিকেল কনচৌল"

msgid "Wait for domain to start"
msgstr "ডমেইন আৰম্ভ হবলে অপেক্ষা কৰক"

#, c-format
msgid "Waiting for display %d..."
msgstr "প্ৰদৰ্শন %d ৰ বাবে অপেক্ষা কৰা হৈ আছে..."

msgid "Waiting for guest domain to be created"
msgstr "অতিথি ডমেইন সৃষ্টি হোৱালে অপেক্ষা কৰা হৈ আছে"

msgid "Waiting for guest domain to re-start"
msgstr "অতিথি ডমেইন পুনৰাম্ভ হোৱাৰ বাবে অপেক্ষা কৰা হৈ আছে"

msgid "Waiting for guest domain to start"
msgstr "অতিথি ডমেইন আৰম্ভ হোৱালে অপেক্ষা কৰা হৈ আছে"

msgid "Waiting for libvirt to start"
msgstr "libvirt আৰম্ভ হবলে অপেক্ষা কৰা হৈছে"

msgid "Zoom _In"
msgstr ""

msgid "Zoom _Out"
msgstr ""

msgctxt "shortcut window"
msgid "Zoom in"
msgstr ""

#, c-format
msgid "Zoom level must be within %d-%d\n"
msgstr "জুম স্তৰ %d-%d ৰ মাজত হব লাগিব\n"

msgid "Zoom level of window, in percentage"
msgstr "উইন্ডোৰ জুম স্তৰ, শতাংশত"

msgctxt "shortcut window"
msgid "Zoom out"
msgstr ""

msgctxt "shortcut window"
msgid "Zoom reset"
msgstr ""

msgid "[none]"
msgstr "[none]"

msgid "_About"
msgstr ""

msgid "_Auto resize"
msgstr ""

msgid "_Cancel"
msgstr ""

msgid "_Close"
msgstr ""

msgid "_Guest details"
msgstr ""

msgid "_Keyboard shortcuts"
msgstr ""

msgid "_Normal Size"
msgstr ""

msgid "_OK"
msgstr ""

msgid "_Pause"
msgstr ""

msgid "_Preferences…"
msgstr ""

msgid "_Reset"
msgstr ""

msgid "_Save"
msgstr ""

msgid "_Screenshot"
msgstr ""

msgid "failed to parse oVirt URI"
msgstr ""

msgid "label"
msgstr "লেবেল"

#, c-format
msgid "oVirt VM %s has no display"
msgstr ""

#, c-format
msgid "oVirt VM %s has no host information"
msgstr ""

#, c-format
msgid "oVirt VM %s has unknown display type: %u"
msgstr ""

#, c-format
msgid "oVirt VM %s is not running"
msgstr ""

msgid "only SSH or UNIX socket connection supported."
msgstr ""

#~ msgid "Disconnect"
#~ msgstr "বিচ্ছিন্ন কৰক"

#~ msgid "Release cursor"
#~ msgstr "উন্মোচন কাৰ্চাৰ"

#~ msgid "Send key combination"
#~ msgstr "চাবি সংযুক্তি পঠাওক"

#~ msgid "Smartcard insertion"
#~ msgstr "স্মাৰ্টকাৰ্ড সোমোৱা"

#~ msgid "Smartcard removal"
#~ msgstr "স্মাৰ্টকাৰ্ড আতৰোৱা"

#~ msgid "_File"
#~ msgstr "ফাইল (_F)"

#~ msgid "_Help"
#~ msgstr "সহায় (_H)"

#~ msgid "_Send key"
#~ msgstr "কি পঠাওক (_S)"

#~ msgid "_View"
#~ msgstr "দৰ্শন কৰক (_V)"

#~ msgid "_Zoom"
#~ msgstr "জুম কৰক (_Z)"

#~ msgid ""
#~ "Copyright (C) 2007-2012 Daniel P. Berrange\n"
#~ "Copyright (C) 2007-2014 Red Hat, Inc."
#~ msgstr ""
#~ "স্বত্বাধিকাৰ (C) ২০০৭-২০১২ Daniel P. Berrange\n"
#~ "স্বত্বাধিকাৰ (C) ২০০৭-২০১৪ Red Hat, Inc."

#~ msgid " "
#~ msgstr " "

#~ msgid "%s%s%s - %s"
#~ msgstr "%s%s%s - %s"

#~ msgid "Connect to ssh failed."
#~ msgstr "ssh লে সংযোগ ব্যৰ্থ হল।"

#~ msgid "Ctrl+Alt"
#~ msgstr "Ctrl+Alt"

#~ msgid "Ctrl+Alt+F11"
#~ msgstr "Ctrl+Alt+F11"

#~ msgid "Ctrl+Alt+F12"
#~ msgstr "Ctrl+Alt+F12"

#~ msgid "Ctrl+Alt+F1_0"
#~ msgstr "Ctrl+Alt+F1_0"

#~ msgid "Ctrl+Alt+F_1"
#~ msgstr "Ctrl+Alt+F_1"

#~ msgid "Ctrl+Alt+F_2"
#~ msgstr "Ctrl+Alt+F_2"

#~ msgid "Ctrl+Alt+F_3"
#~ msgstr "Ctrl+Alt+F_3"

#~ msgid "Ctrl+Alt+F_4"
#~ msgstr "Ctrl+Alt+F_4"

#~ msgid "Ctrl+Alt+F_5"
#~ msgstr "Ctrl+Alt+F_5"

#~ msgid "Ctrl+Alt+F_6"
#~ msgstr "Ctrl+Alt+F_6"

#~ msgid "Ctrl+Alt+F_7"
#~ msgstr "Ctrl+Alt+F_7"

#~ msgid "Ctrl+Alt+F_8"
#~ msgstr "Ctrl+Alt+F_8"

#~ msgid "Ctrl+Alt+F_9"
#~ msgstr "Ctrl+Alt+F_9"

#~ msgid "Ctrl+Alt+_Backspace"
#~ msgstr "Ctrl+Alt+_Backspace"

#~ msgid "Ctrl+Alt+_Del"
#~ msgstr "Ctrl+Alt+_Del"

#~ msgid ""
#~ "This program is free software; you can redistribute it and/or modify\n"
#~ "it under the terms of the GNU General Public License as published by\n"
#~ "the Free Software Foundation; either version 2 of the License, or\n"
#~ "(at your option) any later version.\n"
#~ "\n"
#~ "This program is distributed in the hope that it will be useful,\n"
#~ "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
#~ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
#~ "GNU General Public License for more details.\n"
#~ "\n"
#~ "You should have received a copy of the GNU General Public License\n"
#~ "along with this program; if not, write to the Free Software\n"
#~ "Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  "
#~ "USA\n"
#~ msgstr ""
#~ "এই প্ৰগ্ৰামটো এটা বিনামুলিয়া চফ্টৱেৰ; আপুনি Free Software Foundation ৰ দ্বাৰা "
#~ "প্ৰকাশিত GNU General Public License ৰ চুক্তিসমূহৰ অন্তৰ্গত ইয়াক পুনৰ বিলাব পাৰিব "
#~ "অথবা সলনি  কৰিব পাৰিব; হৈতো অনুজ্ঞাৰ সংস্কৰণ ২, অথবা (আপুনাৰ বিকল্পত) যিকোনো "
#~ "পৰৱৰ্তী সংস্কৰণ।\n"
#~ "\n"
#~ "এই প্ৰগ্ৰামটো এইটো আশাত বিলোৱা হৈছে যে ই ব্যৱহাৰযোগ্য হ'ব, কিন্তু কোনো ৱাৰেন্টি "
#~ "নথকাকৈ; ব্যৱসায়ীক  অথবা কোনো এটা বিশেষ কাৰণৰ যোগ্যতাৰ বাবে বুজুৱা ৱাৰেন্টি  "
#~ "নথকাকৈ। অধিক যানিবলৈ  GNU General Public License চাওক।\n"
#~ "\n"
#~ "আপুনি হৈতো ইতিমধ্যে এই প্ৰগ্ৰামৰ সৈতে GNU General Public License ৰ কপি এটা "
#~ "পাইছে; যদি নাই,  Free Software\n"
#~ "Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA "
#~ "লে লিখক\n"

#~ msgid "virt-manager.org"
#~ msgstr "virt-manager.org"
